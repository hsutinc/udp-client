package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

const version = "0.0.1"

// Port is the port number the UDPClient listen to
var Port string

func main() {
	var rootCmd = &cobra.Command{
		Use:     "udp_client",
		Short:   "udp client for block size simulation test",
		Long:    "udp client for block size simulation test by simulating the process of udp packet transmission via modulator and tv tuner to determine the proper block size",
		Version: version,
		Run: func(_ *cobra.Command, _ []string) {
			fmt.Println("===================================")
			fmt.Printf("Port:\t%s\n", Port)
			fmt.Println("===================================")

			if err := UDPClient(Port); err != nil {
				fmt.Printf("Failed executing UDPClient. %+v\n", err)
			}
		},
	}

	rootCmd.PersistentFlags().StringVarP(&Port, "port", "p", "", "the port udp client listen to")

	rootCmd.MarkPersistentFlagRequired("port")

	if c, err := rootCmd.ExecuteC(); err != nil {
		if c != nil {
			c.Printf("Run '%v --help' for usage.\n", c.CommandPath())
		}
	}
}
