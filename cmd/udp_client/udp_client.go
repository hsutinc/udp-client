package main

import (
	"fmt"
	"net"
	"strings"

	"github.com/pkg/errors"
)

// MaxPacketSize is the max size of a UDP packet
const MaxPacketSize = 65535

// UDPClient is responsible of receiveing and printing recevied UDP packet
func UDPClient(port string) error {
	laddr, err := net.ResolveUDPAddr("udp4", ":"+port)

	if err != nil {
		return errors.Wrap(err, "Failed while resolving address with port")
	}

	conn, err := net.ListenUDP("udp4", laddr)
	if err != nil {
		return errors.Wrapf(err, "Failed while listening UDP from address: %s", laddr)
	}

	defer conn.Close()

	buffer := make([]byte, MaxPacketSize)

	for {
		n, raddr, err := conn.ReadFromUDP(buffer)
		data := string(buffer[0 : n-1])
		if err != nil {
			return errors.Wrap(err, "Failed while reading UDP packet")
		}
		fmt.Printf("-> message from %s: %s\n", raddr, data)
		fmt.Printf("data size: %d\n", len(data))
		fmt.Printf("buffer size: %d\n", n)

		if strings.ToUpper(strings.TrimSpace(data)) == "STOP" {
			fmt.Println("Exiting UDP client")
			break
		}
	}

	return nil
}

// func errHandler(err error, msg string) {
// 	if err != nil {
// 		return errors.Wrap(err, msg)
// 	}
// }
